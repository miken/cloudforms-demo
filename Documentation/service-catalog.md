# Service Catalog

There are four Service Catalogs with multiple Service Catalog Items. A good introduction into managing Service Catalogs can be found in the [Building a self-service portal with Red Hat CloudForms](https://github.com/cbolz/summit-fy19/blob/master/self-service-portal-with-cloudforms/index.md) lab which was originally presented during Red Hat Summit in 2018.

- Ansible
  - Fix SELinux
- Applications
  - CakePHP on OpenShift
  - Wordpress HEAT on OpenStack
- Linux
  - RHEL 7 on OpenStack
  - RHEL 7 on RHV
  - RHEL 7 on vCenter
- Windows
  - Windows 2008 R2

:heavy_check_mark: ***NOTE*** Don't forget to also demonstrate the new Self Service UI accessible by using the URL:

    http://<your hostname>/self_service.

## Ansible: Fix SELinux

This catalog item is used to demonstrate how CloudForms can be used to fix non-compliance status of Virtual Machines. There is an Ansible Tower Job which is configured to use the Playbook [selinux-enforcing.yaml](../Playbooks/selinux-enforcing.yaml). More details about this use cases was already documented in a blog post [Enforce SELinux Compliance Policy with Ansible](http://www.jung-christian.de/post/2017/12/enforce-selinux-with-ansible/).

## Applications: CakePHP on OpenShift

This is a Service Catalog Item using the CakePHP Template shipped with OpenShift. This demonstrates CloudForms' capabilities to create a Service Dialog from the automatically imported OpenShift Template.

## Applications: Wordpress on OpenStack

This is a Service Catalog Item using the Wordpress HEAT Template originally found on the [OpenStack HEAT repository](https://github.com/openstack/heat-templates/tree/master/hot/software-config/example-templates/wordpress). Some fixes were applied and the version used in this demo can be found in the folder [HEAT](../HEAT/).

## Linux: RHEL 7 on OpenStack

This is a simple Service Catalog Item using an RHEL 7 Template to provision an instance on OpenStack.

An intentionally simple Service Dialog is used to show how a simple Service Dialog can look like.

## Linux: RHEL 7 on RHV

This is a simple Service Catalog Item using an RHEL 7 Template to provision a Virtual Machine on Red Hat Virtualization.

The Service Catalog Item demonstrate a more sophisticated Service Dialog which allows the user to select a predefined T-Shirt Size. But we also provide a "Advanced" Tab where the user can override CPU and memory settings.

## Linux: RHEL 7 on vCenter

This is a simple Service Catalog Item using an RHEL 7 Template to provision a Virtual Machine on VMware vCenter.

This Service Catalog Item also demonstrates that CloudForms can indeed show estimated costs in a Service Catalog. This is a quite popular demand. The way this is implemented in this demo is very simple. There are three different T-Shirt sizes and depending on the select, the price estimated at the bottom is recalculated and updated. The code can be found in this repository in the [Automate sub folder](../Automate/). The costs are defined in [estimate_cost](https://gitlab.com/cjung/cloudforms-demo/blob/master/Automate/Integration/DynamicDialogs/methods.class/__methods__/estimate_cost.rb) method.

## Windows: Windows 2008 R2

This is a simple Service Catalog Item using an evaluation version of Windows 2008 R2 Template to provision a Virtual Machine on VMware vCenter.

An intentionally simple Service Dialog is used to show how a simple Service Dialog can look like.