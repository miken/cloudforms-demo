# Ansible Tower

As part of the demo environment an Ansible Tower instance was deployed and is preconfigured.

Ansible Tower is configured to retrieve its inventory from CloudForms by utilizing the dynamic inventory feature.

## Job Templates

A number of Job Templates are preconfigured and ready to use.

- Command: used to run a command on the target machine
- Fix SELinux: enforces "Enforcing" mode for SELinux, check the chapter about Compliance in the [README](README.md)
- HelloWorld: typical hello word example
- Enable Cockpit: installs required packages and enables Cockpit on target machine
- InstallPackage: install a package on the target machine by using yum
- InventoryvCenter: refresh vCenter inventory
- OCP Create Project: creates a project on OpenShift (probably obsolete since CloudForms can now run OpenShift Templates natively)
- Restart Service: restart a given service on the target machine
- Scan Inventory: updates Inventory
- Scan OCP
- Windows InstallPackage: installs given Package on Windows target machine by using [Chocolatey](https://chocolatey.org/)
- Windows Scan

## Use Case: Tools

To demonstrate the capabilities of CloudForms to utilize Ansible Playbooks and the seamless integration of custom buttons, some Ansible Tower Jobs have been created and can be demonstrated with four different buttons. All four buttons can be found in the "Tools" menu on a single Virtual Machine (vCenter or RHV).

- Install Apache: This button is accessible on all VMs, it's greyed out if the VM is powered off and a tooltip is shown (VM has to be running). This can also be used to demonstrate the new feature to make buttons inactive if certain requirements are not met (in this case, to run an Ansible Playbook on the target machine, it has to be powered on)
- Install Package: This is a similar use case, but this time the user can enter the package name in a free text field. This can be used as an example where we allow certain users only specific commands (e.g. operators or VM owners can only install predefined packages, while an admin can install anything).
- Operator Commands: With this button we can demonstrate how certain users can be empowered to run predefined commands, for example an operator might not have root privileges on the target machine, but with this button we empower the operator to still perform certain tasks
- Install Windows Software: This button is only active on Windows VMs, again demonstrating CloudForms' capability to active/inactive buttons based on requirements (in this case the detected Operating System in the target machine). This also demonstrates CloudForms' and Ansible's support for Windows workloads.

## Use Case: Windows Install Package

As said in the chapter "Tools" there is a button to install packages on Windows target machines. Use the "Tools" menu available on a Virtual Machine. This is using an Ansible Tower Job souring the Playbook [Windows-Packages.yaml](../Playbooks/Windows-Packages.yaml).

The button is only visible for Windows Virtual Machines, demonstrating the new capability of CloudForms in dynamically showing or hiding buttons based on certain criteria (in this example, the button is only shown if the Operating System in the target machine is Windows).

This button is also demonstrating the capability to make a button active/inactive if certain requirements are met. In this case, the button is disabled, if the VM is not powered on. Since we use Ansible to run a playbook on the target machine, the VM has to be powered on. CloudForms can show a tooltip providing an explanation why the button is not active "VM has to be powered on").

:heavy_check_mark: ***NOTE*** You have to manually configure some winrm parameters until this is solved properly. See the section on [Windows Tower Jobs](ansible-tower-windows.md)-

## Use Case: Cockpit

There is a custom button labeled "Enable Cockpit" in the "Tools" menu. After clicking this button, an Ansible Tower Job is triggered which will run the [enable-cockpit](https://gitlab.com/cjung/cloudforms-demo/blob/master/Playbooks/enable-cockpit.yaml) Playbook. This Playbook enables the necessary repositories, installs the packages, starts the service and adds the required firewall rules.

After the Playbook complete, reload the Virtual Machine Details page and click on "Access", "Web Console". This will open a new browser tab and - after an additional login - show the Cockpit page.

:warning: ***WARNING*** This demo will only work if you use sshuttle to get VPN access into your blueprint! The Virtual Machine running inside the demo environment, is intentionally not reachable from the internet.

### VPN with SSHuttle

To be able to demonstrate Cockpit, you need access to the internal IP of the Virtual Machine, which is on an 192.168.0.0/24 network. To get VPN access use [sshuttle](https://github.com/sshuttle/sshuttle) (use "dnf install sshuttle" on Fedora or follow instructions):

    sshuttle -r <Bastion Host/Workstation> 192.168.0.0/24

Keep the SSH tunnel open during the demo.

## Use Case: Create Windows User

This custom button allows an VM owner to create a new local user account on a Windows Machine. This Use Case allows to demonstrate how buttons can only be visible if certain requirements are met (in this case, the guest OS is Windows). The button is also only active, if the VM is powered on - otherwise a tool tip will inform the user why the button is not active.

The Tower Job "Windows Create User" takes three parameters: the username, the full name and a password.

:heavy_check_mark: ***NOTE*** You have to manually configure some winrm parameters until this is solved properly. See the section on [Windows Tower Jobs](ansible-tower-windows.md)-