# Windows Tower Jobs

The current version of the Blueprint requires some manual steps to run Windows Ansible Tower Jobs. Make sure to follow these manual steps before demonstrating any of the Windows based Ansible Playbooks.

1. Make sure at least one Virtual Machine with a Windows Guest OS is running

1. Open the Tower Web UI

1. Navigate to ***Inventories*** -> ***CloudForms*** -> ***Groups***

1. Click on the ***tags*** -> ***_managed_os_windows*** group

1. Switch to the details tab

1. Add the following YAML code

        ansible_port: 5986
        ansible_connection: winrm
        ansible_winrm_server_cert_validation: ignore

1. Save the changes

Now you're good to run Windows based actions.