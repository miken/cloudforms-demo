# Control Policies

A scenario to check and enforce Virtual Machine Compliance has be implemented, by combining a Job Template configured on Ansible Tower with a Compliance and a Control Policy on CloudForms.

This use case was documented in the following blog posts:

- [Enforce SELinux Compliance Policy with Ansible](http://www.jung-christian.de/post/2017/12/enforce-selinux-with-ansible/)
- [SELinux Compliance Policy](http://www.jung-christian.de/post/2017/10/control-policy-selinux/)
- [Custom Smart State Analysis Profiles](http://www.jung-christian.de/post/2017/10/modify-ssa-profile/)
- ESX host compliance: Make sure Smart State Analysis was at least once executed on your ESX hosts. This Policy will check if the advanced setting "Syslog.global.defaultRotate" is set to 8. If not, the ESX host is marked as non-compliant.