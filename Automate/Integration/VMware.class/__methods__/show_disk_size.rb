#
# show curent disk size for "Grow Disk" method
#
# Copyright (C) 2017, Christian Jung
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

exit if $evm.root['dialog_disk_name'].blank?
vm = $evm.root['vm']
disk = $evm.vmdb(:disk).where(["hardware_id = ? AND device_name = ?", vm.hardware.id, $evm.root['dialog_disk_name']]).first or 
		raise "Can't find disk #{$evm.root['dialog_disk_name']} for VM #{vm.name}"
disk_size = disk.size / 1024**3
dialog_field = $evm.object
dialog_field["value"] = disk_size
